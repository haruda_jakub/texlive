#! /bin/sh
v=20200406
s=texlive-$v-source.tar.xz
p=${s%.tar.xz}

echo create the clean texlive source, please wait...
# get the texlive original source
if [ ! -f $s ] ; then
  wget http://ftp.tu-chemnitz.de/pub/tug/historic/systems/texlive/2020/$s
  if [ ! -f $s ] ; then
    echo download $s failed!
    exit 1
  fi
fi
tar Jxf $s
# remove files with bad licenses, don't embeded zziplib but build with system lib
rm -f $p/libs/zziplib/zziplib-src/docs/zzipdoc/{options.py,match.py}

# create clean texlive source
tar Jcvf $p-clean.tar.xz $p
echo clean texlive source $p-clean.tar.xz is created.
